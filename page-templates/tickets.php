<?php
/**
 * Template Name: tickets
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>
<!-- content start -->

<div class="page-container">
	<div class="content-container">
		<div class="row">
			<div class="col-md-12">
				<h1>Tickets</h1>
				<h2>Buy tickets for the conference</h2>
				<p>bloop</p>
			</div>
		</div>
	</div>
</div>

<!-- content end -->
<?php

get_footer();
