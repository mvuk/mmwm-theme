<?php
/**
 * Template Name: home
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>
<!-- content start -->

<div class="page-container">
	<div class="content-container">
		<div class="row">
			<div class="col-md-12">
				<h1>Mapping the Mind with Mushrooms 2018</h1>
				<h2>An annual conference that brings together psychologists, philosophers and mycologists to address the current research and findings about psychedelic mushrooms.</h2>
				<div class="cta mobile">
												<!-- TODO https://tympanus.net/Development/ParticleEffectsButtons/ -->
					<a href="/event/mapping-the-mind-with-mushrooms/" class="a-cta">Buy Tickets <i class="far fa-ticket-alt"></i></a>
				</div>
				<p>Over the last 20 years, psilocybin, LSD, MDMA and more have returned to scientific labratories and have been informing neuroscience and therapeutic practice at an unprecedented rate.</p>
				<p>This event is organized by the University of Toronto chaper of <a href="#" class="linkOut">CSSDP</a>, a grassroots network comprised of youth and students who are concerned about the negative impact our drug policies have on individuals and communities.</p>
			</div>
		</div>
	</div>
</div>

<!-- content end -->
<?php

get_footer();
